import 'package:flutter/material.dart';
import 'package:ghrui/save_location.dart';


import 'landing_page.dart';

class AddLocation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Office Anywhere",
          style: TextStyle(color: Colors.white),
        ),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => Landing()));
            }),
        backgroundColor: Colors.lightBlue.shade800,
        elevation: 2,
      ),
      floatingActionButton: new FloatingActionButton.extended(
        backgroundColor: Colors.lightBlue.shade800,
        elevation: 10,
        onPressed: () {
          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SaveLocation()));
        },

        icon: Icon(Icons.chevron_right),
        label: Text('Next'),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0, top: 8.0),
              child: Text(
                "Your Current location :",
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              padding: EdgeInsets.all(30.0),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black45,
                  width: 1,
                ),
            ),
            ),
          ],
        ),
      ),
    );
  }
}


