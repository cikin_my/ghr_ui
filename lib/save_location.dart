import 'package:flutter/material.dart';
import 'package:ghrui/office_anywhere.dart';


class SaveLocation extends StatelessWidget {

  Widget _myform(BuildContext context) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              left: 8.0, right: 8.0, bottom: 8.0, top: 8.0),
                          child: Text(
                            "Save Work location",
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 8.0, right: 8.0, bottom: 10.0, top: 5.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              //===choose place
                              TextFormField(
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.black,
                                ),
                                decoration: InputDecoration(
                                  labelText: "Home ",
                                  fillColor: Colors.white,
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 0, horizontal: 20.0),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(),
                                  ),

                                ),
                              ),
                              Divider( height: 8.0, ),
                              Text("(Note: It will appear on you IM screen too)", textAlign: TextAlign.left,)

                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: SizedBox(
                            width: double.infinity,
                            child: RaisedButton.icon(
                              onPressed: () {
                                // Set intial mode to login
                                Navigator.of(context).pop();
                                Navigator.of(context).push(new MaterialPageRoute(
                                    builder: (BuildContext context) => new OfficeAnywhere()));
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
                              label: Text(
                                'Save',
                                style: TextStyle(
                                    //fontFamily: 'BellotaText',
                                    color: Colors.white,
                                    letterSpacing: 3),
                              ),
                              icon: Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Icon(
                                  Icons.save,
                                ),
                              ),
                              textColor: Colors.white,
                              splashColor: Colors.lightBlue.shade500,
                              color: Colors.lightBlue.shade800,
                            ),
                          ),
                        ),


                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Office Anywhere",
          style: TextStyle(color: Colors.white),
        ),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => OfficeAnywhere()));
            }),
        backgroundColor: Colors.lightBlue.shade800,
        elevation: 2,
      ),
      body: _myform(context) ,
    );
  }
}


