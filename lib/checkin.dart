import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ghrui/landing_page.dart';
import 'package:ghrui/modalPopup/custom_dialog.dart';
import 'animation/fade_animation.dart';
import 'package:intl/intl.dart';

class CheckInOut extends StatefulWidget {
  @override
  _CheckInOutState createState() => _CheckInOutState();
}

class _CheckInOutState extends State<CheckInOut> {
  //variables
  MediaQueryData queryData;
  final now = DateTime.now();


  //  widgets
  Widget _buildButtonCheckIn(data) {

    //later use for styling
        DateTime checkedDay= DateTime.parse(data['Date']);
        DateTime currentDay= DateTime.now();

        int diffDays = checkedDay.difference(currentDay).inDays;
        bool isSame = (diffDays == 0);


    TextStyle _dateTextStyle = TextStyle(
      fontFamily: 'BellotaText',
      color: Colors.green,
      fontSize: 13.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _timeTextStyle = TextStyle(
      fontFamily: 'BellotaText',
      color: Colors.black45,
      fontSize: 13.0,
      fontWeight: FontWeight.normal,
    );
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 8.0, top: 0, bottom: 0),
          child: ButtonTheme(
            minWidth: 50.0,
            height: 29.0,
            child: RaisedButton(
              onPressed: () {
                //check date if its today then disable button
//                if(TodayDate == now) {
//                 do nothing

//                }else{

                  showDialog(
                    context: context,
                    builder: (BuildContext context) =>
                        CustomDialog(
                          title: "Check In",
                          description: "Are you sure you want to Check In now?",
                          primaryButtonText: "Yes",
                          primaryButtonRoute: "/",
                          secondaryButtonText: "Cancel",
                          secondaryButtonRoute: "/check_in_out", //go somewhere
                        ),
                  );
                //}
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(80.0))),
              child: Text(
                'Check In',
                style: TextStyle(
                  //fontFamily: 'BellotaText',
                  color: Colors.white,
                  letterSpacing: 1,
                  fontSize: 12,
                ),
              ),
              textColor: Colors.white,
              splashColor: Colors.lightBlue,
              color: Colors.lightBlue.shade800,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildButtonCheckOut(data) {
    TextStyle _dateTextStyle = TextStyle(
      fontFamily: 'BellotaText',
      color: Colors.green,
      fontSize: 13.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _timeTextStyle = TextStyle(
      fontFamily: 'BellotaText',
      color: Colors.black45,
      fontSize: 13.0,
      fontWeight: FontWeight.normal,
    );
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 8.0, top: 0, bottom: 0),
          child: ButtonTheme(
            minWidth: 50.0,
            height: 29.0,
            child: RaisedButton(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => CustomDialog(
                    title: "Check Out",
                    description: "Are you sure you want to Check Out now?",
                    primaryButtonText: "Yes",
                    primaryButtonRoute: "/",
                    secondaryButtonText: "Cancel",
                    secondaryButtonRoute: "/check_in_out", //go somewhere
                  ),
                );
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(80.0))),
              child: Text(
                'Check Out',
                style: TextStyle(
                  //fontFamily: 'BellotaText',
                  color: Colors.white,
                  letterSpacing: 1,
                  fontSize: 12,
                ),
              ),
              textColor: Colors.white,
              splashColor: Colors.red,
              color: Colors.red.shade800,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildLocationection(data) {
    TextStyle _LocationTextStyle = TextStyle(
      fontFamily: "SourceSansPro",
      letterSpacing: 0.5,
      fontSize: 15,
      fontWeight: FontWeight.bold,
      color: Colors.black,
    );

    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Icon(
          Icons.pin_drop,
          color: Colors.lightBlue,
          size: 20.0,
        ),
        SizedBox(
          width: 5.0,
        ),
        Text(
          data,
          style: _LocationTextStyle,
        ),
      ],
    );
  }

  Widget _buildDateSection(data) {
    TextStyle _DateTextStyle = TextStyle(
      fontFamily: 'SourceSansPro',
      color: Colors.black54,
      fontWeight: FontWeight.bold,
      fontSize: 14,
    );

    TextStyle _DayTextStyle = TextStyle(
      color: Colors.black87,
      fontWeight: FontWeight.bold,
      fontSize: 27,
      fontFamily: "BellotaText",
      letterSpacing: 2,
    );



    return Container(
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(width: 1.0, color: Colors.lightBlue.shade600),
        ),
        color: Colors.white,
      ),
      child: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Column(
          children: <Widget>[
            Text(
              DateFormat('E').format(DateTime.parse(data['Date'])),
              style: _DateTextStyle,
            ),
            Text(
              DateFormat('dd').format(DateTime.parse(data['Date'])),
              style: _DayTextStyle,
            ),
            Text(
              DateFormat('MMM yyyy').format(DateTime.parse(data['Date'])),
              style: TextStyle(
                color: Colors.black54,
                fontWeight: FontWeight.bold,
                fontSize: 12,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _builCheckInOutSection(data) {
    queryData = MediaQuery.of(context);
    final String checkedin = data["checkInTime"];
    final String checkedOut = data["checkOutTime"];

    TextStyle _CheckInTextStyle = TextStyle(
      fontFamily: 'SourceSansPro',
      color: Colors.blueGrey,
      fontSize: 16.0,
    );
    TextStyle _TimeTextStyle = TextStyle(
      fontFamily: 'SourceSansPro',
      color: Colors.blueGrey.shade800,
      fontSize: 16.0,
    );

    return Expanded(
      child: Container(
        child: Padding(
          //**changes start here
          padding: const EdgeInsets.fromLTRB(8, 8, 4, 8),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    _buildLocationection(data["locationName"]),

                    //this is just a test to show different in button visible to user.

                    checkedin != "" && checkedOut != "" || checkedOut == null
                        ? Visibility(
                            child: _buildButtonCheckIn(data),
                            visible: false,
                          )
                        : checkedin != "" && checkedOut == ""
                            ? _buildButtonCheckOut(data)
                            : _buildButtonCheckIn(data),
                  ],
                ),
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: queryData.size.width / 5,
                    child: Text(
                      "Check In",
                      style: (_CheckInTextStyle),
                    ),
                  ),
                  Container(
                      height: 13,
                      child: VerticalDivider(
                        thickness: 1,
                        color: Colors.lightGreen.withOpacity(.5),
                      )),
                  Container(
                    child: Text(
                      data["checkInTime"],
                      style: (_TimeTextStyle),
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                      width: queryData.size.width / 5,
                      child: Text("Check Out", style: (_CheckInTextStyle))),
                  Container(
                      height: 13,
                      child: VerticalDivider(
                        thickness: 1,
                        color: Colors.redAccent.withOpacity(.4),
                      )),
                  Container(
                    child: checkedOut == ""
                        ? Text("--:-- ", style: (TextStyle(letterSpacing: 3)))
                        : checkedOut == null
                            ? Text("You not checkout", style: (_TimeTextStyle))
                            : Text(checkedOut, style: (_TimeTextStyle)),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "CheckOut",
            style: TextStyle(color: Colors.white),
          ),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => Landing()));
              }),
          backgroundColor: Colors.lightBlue.shade800,
          elevation: 2,
        ),
        body: Container(
          child: Center(
            // Use future builder and DefaultAssetBundle to load the local JSON file

            child: FutureBuilder(
                future: DefaultAssetBundle.of(context)
                    .loadString('data/sampleCheckin.json'),
                builder: (context, snapshot) {
                  // Decode the JSON
                  var new_data = jsonDecode(snapshot.data.toString());

                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: new_data == null ? 0 : new_data.length,
                      // Build the ListView
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          margin: new EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Padding(
                            padding: const EdgeInsets.all(0),
                            child: FadeAnimation(
                              1,
                              Card(
                                shadowColor: Colors.black38,
                                elevation: 5.5,
                                clipBehavior: Clip.antiAlias,
//                                shape: RoundedRectangleBorder(
//                                    borderRadius: BorderRadius.circular(8.0)),
                                color: Colors.white,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    //========================================================
                                    //            1.date
                                    //========================================================
                                    _buildDateSection(new_data[index]),
                                    //========================================================
                                    //2. location,check
                                    //========================================================
                                    _builCheckInOutSection(new_data[index]),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }),
          ),
        ));
  }
}
