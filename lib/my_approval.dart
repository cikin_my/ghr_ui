import 'dart:convert';

import 'package:flutter/material.dart';
import 'animation/fade_animation.dart';

class MyApproval extends StatefulWidget {
  @override
  _MyApprovalState createState() => _MyApprovalState();
}

class _MyApprovalState extends State<MyApproval> {
  List data;


  Widget _buildNameSection() {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.black,
      fontSize: 15.0,
      fontWeight: FontWeight.w700,
    );

    return Row(
      children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(18,18,8,10),
            child: CircleAvatar(
              backgroundImage: AssetImage('assets/images/boss.jpg'),
              radius: 18.0,
            ),
          ),
        ),
        Container(
          child: Text(
            "Khairul Shukeri",
            style: _nameTextStyle,
          ),
        ),

      ],
    );
  }
  Widget _buildDateSection() {
    TextStyle _dateTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.black,
      fontSize: 13.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _timeTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.black,
      fontSize: 13.0,
      fontWeight: FontWeight.normal,
    );
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left:18.0,),
          child: Text(
            "1-5 May 2020",
            style: _dateTextStyle,

          ),

        ),

        Padding(
          padding: const EdgeInsets.only(left:18.0),
          child: Text(
            "8 am - 5pm Daily",
            style: _timeTextStyle,
          ),
        ),

      ],
    );
  }
  Widget _buildAppliedSection() {
    TextStyle _dateTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.green,
      fontSize: 13.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _timeTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.black45,
      fontSize: 12.0,
      fontWeight: FontWeight.normal,
    );
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right:18),
          child: Text(
            "Applied for 5 days",
            style: _dateTextStyle,


          ),

        ),

        Padding(
          padding: const EdgeInsets.only(right:18.0),
          child: Text(
            "12 May 2020",
            style: _timeTextStyle,
          ),
        ),

      ],
    );
  }
  Widget _buildLocationSection() {

    TextStyle _locationTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.black45,
      fontSize: 12.0,
      fontWeight: FontWeight.bold,
    );
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[

        Padding(
          padding: const EdgeInsets.only(left:18.0),
          child: Row(
            children: <Widget>[
              Icon(Icons.pin_drop,size: 16,color:Colors.grey,),
              Text(
                "Home",
                style: _locationTextStyle,
              ),


            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 35.0,top: 5),
          child: Text(
            "18, Jalan 1, Taman Gombak",
            style: _locationTextStyle,
          ),
        ),

      ],
    );
  }
  Widget _buildButtonSection() {
    TextStyle _dateTextStyle = TextStyle(
      fontFamily: 'BellotaText',
      color: Colors.green,
      fontSize: 13.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _timeTextStyle = TextStyle(
      fontFamily: 'BellotaText',
      color: Colors.black45,
      fontSize: 13.0,
      fontWeight: FontWeight.normal,
    );
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right:8.0),
          child: ButtonTheme(
            minWidth: 70.0,
            height: 30.0,
            child: RaisedButton.icon(
              onPressed: () {

              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(80.0))),
              label: Text(
                'Approved',
                style: TextStyle(
                  //fontFamily: 'BellotaText',
                    color: Colors.white,
                    letterSpacing: 2,
                fontSize: 13,),
              ),
              icon: Padding(
                padding: EdgeInsets.all(2.0),
                child: Icon(
                  Icons.check, size: 14,
                ),
              ),
              textColor: Colors.white,
              splashColor: Colors.green.shade800,
              color: Colors.green,
            ),
          ),
        ),

      ],
    );
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(

        body: Container(
          child: Center(
            // Use future builder and DefaultAssetBundle to load the local JSON file

            child: FutureBuilder(
                future: DefaultAssetBundle.of(context)
                    .loadString('data/sampleData.json'),
                builder: (context, snapshot) {
                  // Decode the JSON
                  var _data = jsonDecode(snapshot.data.toString());

                  return ListView.builder(
                    itemCount: _data == null ? 0 : _data.length,
                    // Build the ListView
                    itemBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FadeAnimation(
                        1,
                        Card(
                          elevation: 8.5,
                          child: Column(
                            children: [
                              //======================
                              // 1st section
                              //======================
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    //==============left section
                                    _buildNameSection()
                                    //==============right section

                                  ],
                                ),

                              ),


                              //======================
                              // 2nd section
                              //======================
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[

                                    // =====   Left section  ======
                                    _buildDateSection(),

                                    // =====   Right section  ======
                                    _buildAppliedSection()

                                  ],
                                ),

                              ),


                              //======================
                              // 3rd section
                              //======================
                              SizedBox(height: 8.0,),
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[

                                    // =====   Left section  ======
                                    _buildLocationSection(),

                                    // =====   Right section  ======
                                    _buildButtonSection()

                                  ],
                                ),

                              ),
                              SizedBox(height: 8.0,),

                            ],
                          ),
                        ),),
                      );
                    },
                  );
                }),
          ),
        ));
  }
}
