import 'package:flutter/material.dart';
import 'package:ghrui/check_in_out.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';

class IndicatorEg extends StatefulWidget {
  @override
  _IndicatorEgState createState() => _IndicatorEgState();
}

class _IndicatorEgState extends State<IndicatorEg>
    with TickerProviderStateMixin {

  Widget _buildPendingList(){
    return Center(
      child: Container(child: Text("Pending list"),
      ),
    );
  }
  Widget _buildApprovedList(){
    return Center(
      child: Container(child: Text("Approved list"),
      ),
    );
  }
  Widget _buildDeclinedList(){
    return Center(
      child: Container(child: Text("Declined list"),
      ),
    );
  }




  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text('MyApproval',style: (TextStyle(color: Colors.black)),),
        bottom: TabBar(
          isScrollable: true,
          unselectedLabelColor: Colors.grey,
          labelColor: Colors.white,
          indicatorSize: TabBarIndicatorSize.tab,
          indicator: BubbleTabIndicator(
            indicatorHeight: 29.0,
            indicatorColor: Colors.lightBlue.shade800,
            tabBarIndicatorSize: TabBarIndicatorSize.tab,
          ),
            tabs: [
              Tab(
                child: Align(
                  alignment: Alignment.center,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.watch_later, size: 15,),
                      SizedBox(width: 5.0,),
                      Text("Pending",style: TextStyle(fontSize: 15,letterSpacing: .5),),
                    ],
                  ),
                ),
              ),
              Tab(
                child: Align(
                  alignment: Alignment.center,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.check, size: 15,),
                      SizedBox(width: 5.0,),
                      Text("Approved",style: TextStyle(fontSize: 15,letterSpacing: .5),),
                    ],
                  ),
                ),
              ),
              Tab(
                child: Align(
                  alignment: Alignment.center,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.clear, size: 15,),
                      SizedBox(width: 5.0,),
                      Text("Decline",style: TextStyle(fontSize: 15,letterSpacing: .5),),
                    ],
                  ),
                ),
              ),
            ],

        ),
      ),
      body: TabBarView(children: [
        _buildPendingList(),
        _buildApprovedList(),
        _buildDeclinedList(),

      ]),
    ),
    );
  }
}
