import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ghrui/business_card.dart';
import 'package:ghrui/login.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyDrawer2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            padding: EdgeInsets.fromLTRB(18, 10, 8, 0),
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.centerLeft,
                          child: CircleAvatar(
                            backgroundImage: AssetImage('assets/images/me.png'),
                            radius: 50.0,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      8.0, 20.0, 8.0, 0),
                                  child: Text(
                                    'Shukeri Saleh',
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontSize: 15.0,
                                        fontFamily: "OpenSans"),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      8.0, 1.0, 8.0, 0),
                                  child: Text(
                                    'Manager',
                                    style: TextStyle(
                                        color: Colors.black45,
                                        fontSize: 13.0,
                                        fontFamily: "OpenSans"),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: IconButton(
                            icon: Icon(
                              Icons.settings,
                              color: Colors.blue,
                              size: 18.0,
                            ),
                            onPressed: () {},
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    FlatButton.icon(
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) => new businessCard()));
                        },
                        icon: FaIcon(
                          FontAwesomeIcons.qrcode,
                          color: Colors.blue,
                          size: 12.0,
                        ),
                        label: Text(
                          "Digital IDs",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blue,
                              fontSize: 13.0,
                              fontFamily: "OpenSans"),
                        )),
                  ],
                )
              ],
            ),
          ),
          ExpansionTile(
            title: Text("Business Card Details"),
            children: <Widget>[
              ListTile(
                title: Text(
                  "Name",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                subtitle: Text(
                  "Shukeri Saleh",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                dense: true,
                onTap: () {},
              ),
              new Divider(height: 1),
              ListTile(
                title: Text(
                  "Job Title",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                subtitle: Text(
                  "Manager ",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                dense: true,
                onTap: () {},
              ),
              new Divider(height: 1),
              ListTile(
                title: Text(
                  "Organization",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                subtitle: Text(
                  "TNB",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                dense: true,
                onTap: () {},
              ),
              new Divider(height: 1),
              ListTile(
                title: Text(
                  "Department",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                subtitle: Text(
                  "ICT Digital Delivery ",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                dense: true,
                onTap: () {},
              ),
              new Divider(height: 1),
              ListTile(
                title: Text(
                  "Email",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                subtitle: Text(
                  "KhairulShukeri@tnb.com.my",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                dense: true,
                onTap: () {},
              ),
              new Divider(height: 1),
              ListTile(
                title: Text(
                  "Mobile No",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                subtitle: Text(
                  "+6017-5587882",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                dense: true,
                onTap: () {},
              ),
              new Divider(height: 1),
              ListTile(
                title: Text(
                  "Office No",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                subtitle: Text(
                  "+03-22466120",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                dense: true,
                onTap: () {},
              ),
              new Divider(height: 1),
            ],
          ),
          new Divider(
            height: 1,
          ),
          ExpansionTile(
            title: Text("Employee Details"),
            children: <Widget>[
              ListTile(
                title: Text(
                  "Immediate Manager",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                subtitle: Text(
                  "Shukeri Saleh",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                dense: true,
                onTap: () {},
              ),
              new Divider(height: 1),
              ListTile(
                title: Text(
                  "StaffID",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                subtitle: Text(
                  "8700908009 ",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                dense: true,
                onTap: () {},
              ),
              new Divider(height: 1),
              ListTile(
                title: Text(
                  "Organization",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                subtitle: Text(
                  "TNB",
                  style: TextStyle(fontFamily: "OpenSans"),
                ),
                dense: true,
                onTap: () {},
              ),
            ],
          ),
          new Divider(
            height: 1,
          ),
          new ListTile(
            trailing: new Icon(Icons.settings_power),
            title: new Text("Log Out"),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new Login()));
            },
          ),
        ],
      ),
    );
  }
}
