import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ghrui/landing_page.dart';
import 'package:ghrui/modalPopup/custom_dialog.dart';
import 'animation/fade_animation.dart';
import 'package:intl/intl.dart';

class MyTeam extends StatefulWidget {
  @override
  _MyTeamState createState() => _MyTeamState();
}

class _MyTeamState extends State<MyTeam> {
  //variables
  MediaQueryData queryData;


  Widget _buildLocation(data) {
    TextStyle _LocationTextStyle = TextStyle(
      fontFamily: "SourceSansPro",
      fontSize: 14,
      fontWeight: FontWeight.bold,
      color: Colors.black45,
    );

    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Icon(
          Icons.pin_drop,
          color: Colors.lightBlue.shade100,
          size: 16.0,
        ),
        SizedBox(
          width: 5.0,
        ),
        Text(
          data,
          style: _LocationTextStyle,
        ),
      ],
    );
  }

  Widget _buildName(data) {
    return Row(
      children: <Widget>[
        Icon(
          Icons.account_circle,
          color: Colors.grey,
          size: 15,
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          data["name"],
          style: (TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.bold,
              letterSpacing: .5,
              fontFamily: "SourceSansPro")),
        ),
      ],
    );
  }

  Widget _builMyTeamSection(data) {
    queryData = MediaQuery.of(context);
    final String checkedin = data["checkInTime"];
    final String checkedOut = data["checkOutTime"];

    TextStyle _CheckInTextStyle = TextStyle(
      fontFamily: 'SourceSansPro',
      color: Colors.blueGrey,
      fontSize: 16.0,
    );
    TextStyle _TimeTextStyle = TextStyle(
      fontFamily: 'SourceSansPro',
      color: Colors.blueGrey.shade800,
      fontSize: 16.0,
    );

    return Expanded(
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  _buildName(data),

                  //remove this if user is not IM
                    Icon(
                      Icons.chevron_right,
                      color: Colors.lightBlue.shade800,
                    )


                ],
              ),
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: _buildLocation(data["locationName"]),
                  ),
                ],
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: queryData.size.width / 5,
                    child: Text(
                      "Check In",
                      style: (_CheckInTextStyle),
                    ),
                  ),
                  Container(
                      height: 13,
                      child: VerticalDivider(
                        thickness: 1,
                        color: Colors.lightGreen.withOpacity(.5),
                      )),
                  Container(
                    child: Text(
                      checkedin,
                      style: (_TimeTextStyle),
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                      width: queryData.size.width / 5,
                      child: Text("Check Out", style: (_CheckInTextStyle))),
                  Container(
                      height: 13,
                      child: VerticalDivider(
                        thickness: 1,
                        color: Colors.redAccent.withOpacity(.4),
                      )),
                  Container(
                    child: checkedOut == ""
                        ? Text("--:-- ", style: (TextStyle(letterSpacing: 3)))
                        : checkedOut == null
                            ? Text("You not checkout", style: (_TimeTextStyle))
                            : Text(checkedOut, style: (_TimeTextStyle)),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text('My Team', style: TextStyle(
            fontFamily: "SourceSansPro",
            fontSize: 18,
            letterSpacing: 1,
            color: Colors.blueGrey.shade900,
          ),),
          iconTheme: new IconThemeData(color: Colors.lightBlue.shade800),
          backgroundColor: Colors.white,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => Landing()));
              }),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.date_range,

                color: Colors.blue.shade700,
              ),
              tooltip: 'Show Snackbar',
              onPressed: () {},
            ),

          ],

        ),
        body: Container(
          child: FutureBuilder(
              future: DefaultAssetBundle.of(context)
                  .loadString('data/myteamdata.json'),
              builder: (context, snapshot) {
                // Decode the JSON
                var new_data = jsonDecode(snapshot.data.toString());
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: new_data == null ? 0 : new_data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        child: FadeAnimation(
                          1,
                          Card(
                            shadowColor: Colors.black38,
                            elevation: 5.5,
                            clipBehavior: Clip.antiAlias,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)),
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.only(left:18.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  _builMyTeamSection(new_data[index]),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                );
              }),
        ));
  }
}
