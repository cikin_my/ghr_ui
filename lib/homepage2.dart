import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomePage2 extends StatefulWidget {
  @override
  _HomePage2State createState() => _HomePage2State();
}

class _HomePage2State extends State<HomePage2> {
  double screenHeight;
  final String _text1 =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit ";

  Widget _buildBio(BuildContext context) {
    TextStyle bioTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.grey.shade800,
      fontSize: 16.0,
      letterSpacing: 1,

    );
    return Container(
      //color: Theme.of(context).scaffoldBackgroundColor,
      padding: EdgeInsets.all(18.0),
      child: Text(
        _text1,
        textAlign: TextAlign.center,
        style: bioTextStyle,
      ),
    );
  }

  Widget _extraContent(BuildContext context) {
    TextStyle bioTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      fontWeight: FontWeight.w900,
      color: Colors.black,
      fontSize: 18.0,
    );
    return Container(
      padding: EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Self Health Declaration",
            textAlign: TextAlign.left,
            style: bioTextStyle,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            "Please remember to declare your health as we transit back to work",
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 16,
              fontWeight:FontWeight.w600,
              color: Colors.black54,
              fontFamily: "OpenSans",
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomPadding: true,
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    elevation: 4,
                    child: Padding(
                      padding: const EdgeInsets.all(18.0),
                      child: Column(
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              Center(
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      child: Image.asset("assets/images/home1.jpg"),
                                    ),
                                  ],
                                ),
                              ),
//                              Align(
//                                alignment: Alignment.topLeft,
//                                child: Padding(
//                                  padding: const EdgeInsets.only(bottom: 10.0),
//                                  child: Container(
//                                    child: Image.asset("assets/images/tnb_full.png",
//                                        width: 70.5,
//                                        height: 70.5,
//                                        color: Color.fromRGBO(255, 255, 255, 0.8),
//                                        colorBlendMode: BlendMode.modulate),
//                                  ),
//                                ),
//                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    SizedBox(height: 20,),
                                    Text(
                                      "Welcome to  a",
                                      style: TextStyle(
                                          color: Colors.lightBlue.shade800,
                                          fontFamily: "OpenSans",
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.center,
                                    ),
                                    Text(
                                      "new TNB engagement!",
                                      style: TextStyle(
                                          color: Colors.lightBlue.shade800,
                                          fontFamily: "OpenSans",
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.center,
                                    ),
                                    _buildBio(context),

                                Image.asset("assets/images/tnb_full.png",
                                        width: 70.5,
                                        height: 70.5,
                                        color: Color.fromRGBO(255, 255, 255, 0.8),
                                        colorBlendMode: BlendMode.modulate),

                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),

              ],
            ),
          ),
        ));
  }
}
