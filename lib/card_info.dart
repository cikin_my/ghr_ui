import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CardInfo extends StatefulWidget {
  @override
  _CardInfoState createState() => _CardInfoState();
}

class _CardInfoState extends State<CardInfo> {

  final String _fullName = "Zhafri";
  final String _email = "ictdigitaldelivery@gmail.com";



  Widget _buildCoverImage(Size screenSize) {
    return Container(
      height: screenSize.height / 6.6,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/bg1.jpg'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _buildProfileImage() {
    return Center(
      child: Container(
        width: 100.0,
        height: 100.0,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/me.png'),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.circular(80.0),
          border: Border.all(
            color: Colors.white,
            width: 5.0,
          ),
        ),
      ),
    );
  }

  Widget _buildFullName() {
    TextStyle _nameTextStyle = TextStyle(
      //fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 22.0,
      fontWeight: FontWeight.w700,
    );

    return Text(
      _fullName,
      style: _nameTextStyle,
    );
  }
  Widget _buildEmail(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(1.0),
      child: Text(
        _email,
        style: TextStyle(
          //fontFamily: 'Spectral',
          color: Colors.black,
          fontSize: 15.0,
          fontWeight: FontWeight.w300,
        ),
      ),
    );
  }


  Widget _buildIMName() {
    TextStyle _nameTextStyle = TextStyle(
      //fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 17.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _emailTextStyle = TextStyle(
      //fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 14.0,
      fontWeight: FontWeight.normal,
    );

    return Row(
      children: <Widget>[
        Column(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: CircleAvatar(
                  backgroundImage: AssetImage('assets/images/boss.jpg'),
                  radius: 25.0,
                ),
              ),
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Text(
                "Shuk",
                style: _nameTextStyle,
              ),
            ),
            Text(
              "mkshukeri@gmail.com",
              style: _emailTextStyle,
            ),
          ],
        ),
      ],
    );
  }
  Widget _buildIMTitle() {
    return Container(
      padding: EdgeInsets.fromLTRB(8, 20, 8, 8),
      child: Text(
        "Immediate Manager Information",
        style: TextStyle(
          //fontFamily: 'Spectral',
          color: Colors.lightBlue.shade800,
          fontSize: 16.0,
          letterSpacing: 1,

        ),
      ),
    );
  }



  Widget _buildButtons() {
    return Padding(
      padding: EdgeInsets.all(18.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: InkWell(
              onTap: () => print("followed"),
              child:  Padding(
                padding: const EdgeInsets.all(10.0),
                child: SizedBox(
                  width: double.infinity,
                  child: RaisedButton.icon(
                    onPressed: () {

                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20.0))),
                    label: Text(
                      'Remove',
                      style: TextStyle(
                        //fontFamily: 'BellotaText',
                          color: Colors.white,
                          letterSpacing: 3),
                    ),
                    icon: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Icon(
                        Icons.delete,
                      ),
                    ),
                    textColor: Colors.white,
                    splashColor: Colors.lightBlue.shade500,
                    color: Colors.lightBlue.shade800,
                  ),
                ),
              ),
            ),
          ),

        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(title: Text("this is profile"),),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child:
        ListView(
          children: <Widget>[
            Card(
              elevation: 8,
              child: Stack(
                children: <Widget>[
                  _buildCoverImage(screenSize),
                  SafeArea(
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: screenSize.height / 13),
                          _buildProfileImage(),
                          SizedBox(height: 20.0,),
                          _buildFullName(),
                          SizedBox(height: 5.0,),
                          _buildEmail(context),
                          SizedBox(height: 10.0,),
                          Divider(),
                          _buildIMTitle(),
                          _buildIMName(),
                          _buildButtons(),

                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),


          ],
        ),
      ),
    );
  }
}