import 'package:flutter/material.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';
import 'animation/fade_animation.dart';
import 'landing_page.dart';

class MyBg extends StatefulWidget {
  @override
  _MyBgState createState() => _MyBgState();
}

class _MyBgState extends State<MyBg> {
// Initially password is obscure
  bool _obscureText = true;
  double screenHeight;

  Widget _waveBackground() {
    return WaveWidget(
      config: CustomConfig(
        colors: [
//          Color.fromRGBO(59, 82, 186, 0.2),
//          Color.fromRGBO(59, 82, 186, 0.4),
//          Color.fromRGBO(59, 82, 186, 0.6),
//          Color.fromRGBO(239, 241, 250, 0),
          Color.fromRGBO(1, 87, 155, 0.2),
          Color.fromRGBO(1, 88, 142, 0.4),
          Color.fromRGBO(1, 89, 155, 0.6),
          Color.fromRGBO(1, 89, 155, 0.3),
        ],
        durations: [32000, 21000, 18000, 5000],
        heightPercentages: [0.20, 0.23, 0.25, 0.30],
      ),
      backgroundColor: Colors.lightBlue.shade800,
      size: Size(double.infinity, double.infinity),
      waveAmplitude: 0,
    );
  }
  Widget _buildLoginForm(){
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30))),
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(30),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                FadeAnimation(
                    1.4,
                    Container(
                      child: Column(
                        children: <Widget>[
                          new Text(
                            "Welcome !",
                            style: TextStyle(
                                fontSize: 26.0,
                                color: Colors.lightBlue.shade800,
                                fontWeight: FontWeight.bold,
                                fontFamily: "OpenSans",
                                letterSpacing: 1),
                          ),
                          SizedBox(
                            height: 50.0,
                          ),
                          new TextFormField(
                            style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.black87,
                            ),
                            decoration: new InputDecoration(
                              prefixIcon: Padding(
                                  padding: EdgeInsets.all(0.0),
                                  child: Icon(
                                    Icons.person,
                                    color: Colors.black26,
                                  ) // icon is 48px widget.
                              ),
                              labelText: "Username",
                              fillColor: Colors.white,
                              contentPadding:
                              new EdgeInsets.symmetric(
                                  vertical: 0,
                                  horizontal: 20.0),
                              border: new OutlineInputBorder(
                                borderRadius:
                                new BorderRadius.circular(15.0),
                                borderSide: new BorderSide(),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          new TextFormField(
                            style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.black87,
                            ),
                            obscureText: _obscureText,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              prefixIcon: Padding(
                                  padding: EdgeInsets.all(0.0),
                                  child: Icon(
                                    Icons.vpn_key,
                                    color: Colors.black26,
                                  ) // icon is 48px widget.
                              ),
                              hintText: 'Password',
                              contentPadding: EdgeInsets.fromLTRB(
                                  20.0, 10.0, 20.0, 10.0),
                              border: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.circular(32.0)),
                              suffixIcon: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _obscureText = !_obscureText;
                                  });
                                },
                                child: Icon(
                                  _obscureText
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  semanticLabel: _obscureText
                                      ? 'show password'
                                      : 'hide password',
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )),
                SizedBox(
                  height: 20,
                ),
                FlatButton(
                  child: FadeAnimation(
                      1.5,
                      Text(
                        "Forgot your Password?",
                        style: TextStyle(
                          color: Colors.grey,
                          fontFamily: "OpenSans",
                        ),
                      )),
                  onPressed: () {},
                ),
                SizedBox(
                  height: 30,
                ),
                FlatButton(
                  child: FadeAnimation(
                      1.6,
                      Container(
                        height: 50,
                        margin:
                        EdgeInsets.symmetric(horizontal: 80),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.lightBlue[900]),
                        child: Center(
                          child: Text(
                            "LOGIN",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                letterSpacing: 2,
                                fontSize: 13.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      )),
                  onPressed: () {
                    Navigator.of(context).push(
                        new MaterialPageRoute(
                            builder: (BuildContext context) =>
                            new Landing()));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
              child: _waveBackground()),
          Container(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: screenHeight/18),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              FadeAnimation(
                                  1,
                                  Text(
                                    " Enterprise",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 26,
                                        letterSpacing: 1.5,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "OpenSans"),
                                  )),
                              SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              FadeAnimation(
                                  1,
                                  Text(
                                    " Employee App",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 26,
                                        letterSpacing: 1.5,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "OpenSans"),
                                  )),
                              SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              FadeAnimation(
                                  1.3,
                                  Text(
                                    "Latest version 1.1",
                                    style: TextStyle(
                                        color: Colors.white70,
                                        fontSize: 14,
                                        fontFamily: "OpenSans"),
                                  )),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: <Widget>[
                              Image.asset(
                                'assets/images/logo.png',
                                width: 70,
                                height: 70,
                              ),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: screenHeight/20),
                _buildLoginForm(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
