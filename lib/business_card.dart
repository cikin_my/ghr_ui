import 'package:flutter/material.dart';
import 'package:ghrui/landing_page.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

import 'animation/fade_animation.dart';

import 'package:qr_flutter/qr_flutter.dart';
import 'package:carousel_slider/carousel_slider.dart';


class businessCard extends StatefulWidget {
  // Initially password is obscure
  @override
  _businessCardState createState() => _businessCardState();
}

class _businessCardState extends State<businessCard> {
  final List<String> imgList = [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
  ];

  bool _obscureText = true;

  double screenHeight;

  Widget _buildQrCode(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
      decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: QrImage(
        backgroundColor: Colors.white,
        foregroundColor: Colors.blue.shade800,
        padding: EdgeInsets.all(20),
        size: 250,
        data: "https://youtu.be/QMyYWh1b_kU",
      ),
    );
  }

  Widget _waveBackground(){
    return WaveWidget(
      config: CustomConfig(
        colors: [
          Color.fromRGBO(59, 82, 186, 0.2),
          Color.fromRGBO(59, 82, 186, 0.4),
          Color.fromRGBO(59, 82, 186, 0.6),
          Color.fromRGBO(239, 241, 250, 0),
        ],
        durations: [32000, 21000, 18000, 5000],
        heightPercentages: [0.25, 0.26, 0.28, 0.31],
      ),
      backgroundColor: Colors.blue.shade900,
      size: Size(double.infinity, double.infinity),
      waveAmplitude: 0,
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: screenHeight / 4.4),
//            width: double.infinity,
            child: _waveBackground(),
          ),
          Container(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 50),
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              FadeAnimation(
                                  1,
                                  Text(
                                    " Business Card",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 23,
                                        letterSpacing: 1.5,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "OpenSans"),
                                  )),
                              SizedBox(
                                height: 10,
                              ),
                            ],
                          ),

                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.all(30),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10,
                            ),
                            FadeAnimation(
                                1.4,
                                Container(
                                  child: Column(
                                    children: <Widget>[

                                        _buildQrCode(context),

                                    ],
                                  ),
                                )),
                            SizedBox(
                              height: 20,
                            ),
                            FlatButton(
                              child: FadeAnimation(
                                  1.5,
                                  Text(
                                    "Swipe right for Employee Details",
                                    style: TextStyle(color: Colors.grey,fontFamily: "OpenSans",),
                                  )),
                              onPressed: () {},
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            FlatButton(
                              child: FadeAnimation(
                                  1.6,
                                  Container(
                                    height: 50,
                                    margin:
                                    EdgeInsets.symmetric(horizontal: 80),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50),
                                        color: Colors.blue[900]),
                                    child: Center(
                                      child: Text(
                                        "Close",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: "OpenSans",
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  )),
                              onPressed: () {
                                Navigator.of(context).push(
                                    new MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                        new Landing()));
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
