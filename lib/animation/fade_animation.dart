import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:supercharged/supercharged.dart';

// Create enum that defines the animated properties
enum _AniProps { opacity, translateY }

class FadeAnimation extends StatelessWidget {
  final double delay;
  final Widget child;

  FadeAnimation(this.delay, this.child);

  @override
  Widget build(BuildContext context) {
    //this thing is deprecated:
    //    final tween = MultiTrackTween([
    //      Track("opacity").add(Duration(milliseconds: 500), Tween(begin: 0.0, end: 1.0)),
    //      Track("translateY").add(
    //          Duration(milliseconds: 500), Tween(begin: -30.0, end: 0.0),
    //          curve: Curves.easeOut)
    //    ]);
    final tween = MultiTween<_AniProps>()
      ..add(_AniProps.opacity, (0.0).tweenTo(1.0), 500.milliseconds)
      ..add(_AniProps.translateY, (-30.0).tweenTo(0.0), 500.milliseconds,
          Curves.easeOut);

    //this thing is deprecated:
    //    return ControlledAnimation(
    //      delay: Duration(milliseconds: (500 * delay).round()),
    //      duration: tween.duration,
    //      tween: tween,
    //      child: child,
    //      builderWithChild: (context, child, animation) => Opacity(
    //        opacity: animation["opacity"],
    //        child: Transform.translate(
    //            offset: Offset(0, animation["translateY"]),
    //            child: child
    //        ),
    //      ),
    //    );

    return PlayAnimation<MultiTweenValues<_AniProps>>(
      delay: Duration(milliseconds: (500 * delay).round()),
      duration: tween.duration,
      // Obtain duration from MultiTween
      tween: tween,
      child: child,
      builder: (context, child, value) =>
          Opacity(opacity: value.get(_AniProps.opacity), child: child),
    );
  }
}
