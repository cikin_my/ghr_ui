import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'animation/fade_animation.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyApproval2 extends StatefulWidget {
  @override
  _MyApproval2State createState() => _MyApproval2State();
}

class _MyApproval2State extends State<MyApproval2> {
  List data;


//  Widget _buildCalendarSection() {
//
//    return Row(
//      children: <Widget>[
//        Padding(
//          padding: const EdgeInsets.only(top:1.0,right:10.0),
//          child: Container(child: IconButton(
//            icon: FaIcon(FontAwesomeIcons.calendarAlt,color: Colors.lightBlue.shade800,size: 17,),
//            tooltip: 'See MyTeam calendar',
//            onPressed: () {
//            },
//          ),
//
//          ),
//        ),
//
//      ],
//    );
//  }
  Widget _buildDateSection(data) {
    TextStyle _dateTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.black,
      fontSize: 15.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _timeTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.black,
      fontSize: 15.0,
      fontWeight: FontWeight.normal,
    );
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(18,0,2,0),
          child: Text(
            " 1-5 May 2020",
            style: _dateTextStyle,

          ),

        ),
         Padding(
           padding: const EdgeInsets.fromLTRB(2,0,8,0),
           child: Text(
              " | 8 am - 5pm ",
              style: _timeTextStyle,
            ),
         ),



      ],
    );
  }
  Widget _buildAppliedSection(data) {
    TextStyle _dateTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.black,
      fontSize: 12.0,
      fontWeight: FontWeight.w400,
    );
    TextStyle _timeTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.black,
      fontSize: 13.0,
      fontWeight: FontWeight.normal,
      fontStyle: FontStyle.italic,
    );
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(18,8,10,8),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Icon(Icons.watch_later, color: Colors.black26,),
              Row(
               crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[

                  Text(
                    " This request is approved on ",
                    style: _dateTextStyle,
                  ),
                  Text(
                    data['approved'],
                    style: _timeTextStyle,
                  ),

                ],
              ),

            ],
          ),

        ),




      ],
    );
  }
  Widget _buildNameLocationSection(data) {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.black,
      fontSize: 16.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _locationNameTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.black45,
      fontSize: 13.0,
      fontWeight: FontWeight.bold,
    );
    TextStyle _locationTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.black45,
      fontSize: 12.0,
      fontWeight: FontWeight.w400,
    );
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left:18.0),
          child: CircleAvatar(
            backgroundImage: AssetImage('assets/images/boss.jpg'),
            radius: 25.0,
          ),
        ),
        Container(

          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left:18.0),
                child: Text(
                  data['Name'],
                  style: _nameTextStyle,
                ),
              ),
              SizedBox(height: 5.0,),
              Padding(
                padding: const EdgeInsets.only(left:18.0,top:5),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.pin_drop,size: 16,color:Colors.lightBlue.shade800,),
                    Text(
                      data['locationName'],
                      style: _locationNameTextStyle,
                    ),


                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18.0,top:2),
                child: Text(
                  data['locationAddress'],
                  style: _locationTextStyle,
                ),
              ),

            ],
          ),
        ),
      ],
    );
  }
  Widget _buildButtonSection(data) {
    TextStyle _dateTextStyle = TextStyle(
      fontFamily: 'BellotaText',
      color: Colors.green,
      fontSize: 13.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _timeTextStyle = TextStyle(
      fontFamily: 'BellotaText',
      color: Colors.black45,
      fontSize: 13.0,
      fontWeight: FontWeight.normal,
    );
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 8.0 , top: 0),
          child: ButtonTheme(
            minWidth: 50.0,
            height: 23.0,
            child: RaisedButton.icon(
              onPressed: () {

              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(80.0))),
              label: Text(
                'Approved',
                style: TextStyle(
                  //fontFamily: 'BellotaText',
                  color: Colors.white,
                  letterSpacing: 1,
                  fontSize: 12,),
              ),
              icon: Padding(
                padding: EdgeInsets.all(0),
                child: Icon(
                  Icons.check, size: 13,
                ),
              ),
              textColor: Colors.white,
              splashColor: Colors.green.shade800,
              color: Colors.green,
            ),
          ),
        ),

      ],
    );
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(

        body: Container(
          child: Center(
            // Use future builder and DefaultAssetBundle to load the local JSON file

            child: FutureBuilder(
                future: DefaultAssetBundle.of(context)
                    .loadString('data/sampleData.json'),
                builder: (context, snapshot) {
                  // Decode the JSON
                  var new_data  = jsonDecode(snapshot.data.toString());

                  return ListView.builder(
                    itemCount: new_data  == null ? 0 : new_data .length,
                    // Build the ListView
                    itemBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FadeAnimation(
                          1,
                          Card(
                            elevation: 8.5,
                            child: Column(
                              children: [
                                //============================================
                                // 1st section
                                //============================================
                                Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      //==============left section
                                      _buildDateSection(new_data[index]),
                                      _buildButtonSection(new_data[index]),

                                      //==============right section

                                    ],
                                  ),

                                ),
                                Divider(),

                                //============================================
                                // 2nd section
                                //============================================
                                Container(
                                  child: Column(
                                   // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      _buildNameLocationSection(new_data[index]),
                                    ],
                                  ),

                                ),

                                //============================================
                                // 3rd section
                                //============================================
                                SizedBox(height: 8.0,),

                                Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[


                                      _buildAppliedSection(new_data[index]),
                                     // _buildButtonSection()


                                    ],
                                  ),

                                ),
                                SizedBox(height: 8.0,),

                              ],
                            ),
                          ),),
                      );
                    },
                  );
                }),
          ),
        ));
  }
}
