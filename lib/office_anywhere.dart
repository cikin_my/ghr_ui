import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ghrui/landing_page.dart';
import 'package:ghrui/add_location.dart';
import 'package:http/http.dart' as http;

class OfficeAnywhere extends StatefulWidget {
  @override
  _OfficeAnywhereState createState() => _OfficeAnywhereState();
}

class _OfficeAnywhereState extends State<OfficeAnywhere> {
  DateTime selectedDate;
  TimeOfDay time;

  @override
  void initState() {
    super.initState();
    selectedDate = DateTime.now();
    time = TimeOfDay.now();
    _getStateList();
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay timePicked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (timePicked != null)
      setState(() {
        time = timePicked.replacing(hour: timePicked.hourOfPeriod);
      });
  }

  _pickTime() async {
    TimeOfDay t = await showTimePicker(context: context, initialTime: time);
    if (t != null)
      setState(() {
        time = t;
      });
  }

  bool _autovalidate = false;
  String selectedSalutation;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Office Anywhere",
          style: TextStyle(color: Colors.white),
        ),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => Landing()));
            }),
        backgroundColor: Colors.lightBlue.shade800,
        elevation: 2,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            autovalidate: _autovalidate,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 30.0,
                ),
                // 1. location
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0, left: 8.0),
                      child: Icon(
                        FontAwesomeIcons.mapMarkedAlt,
                        color: Colors.lightBlue.shade800,
                      ),
                    ),
                    Text(
                      " Choose your Workspace",
                      style: TextStyle(fontSize: 17),
                    ),
                  ],
                ),
                Row(children: <Widget>[
                  //select location drop down
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(top: 10.0),
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(207, 214, 230, 0.2),
                        border: Border.all(
                          color: Color.fromRGBO(207, 214, 230, 0.2),
                          width: 1,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: Container(
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButtonFormField<String>(
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                              ),
                              value: _myState,
                              iconSize: 30,
                              icon: (null),
                              style: TextStyle(
                                color: Colors.black54,
                                fontSize: 16,
                              ),
                              hint: Text('Select Location'),
                              onChanged: (String newValue) {
                                setState(() {
                                  _myState = newValue;
                                  print(_myState);
                                });
                              },
                              validator: (value) =>
                                  value == null ? 'field required' : null,
                              items: statesList?.map((item) {
                                    return new DropdownMenuItem(
                                      child: new Text(item['name']),
                                      value: item['id'].toString(),
                                    );
                                  })?.toList() ??
                                  [],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ]),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("OR"),
                ),
                // 2. date time
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton.icon(
                      onPressed: () {
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new AddLocation()));
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(20.0))),
                      label: Text(
                        'Add New Place',
                        style: TextStyle(
                            //fontFamily: 'BellotaText',
                            color: Colors.white,
                            letterSpacing: 3),
                      ),
                      icon: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Icon(
                          Icons.pin_drop,
                        ),
                      ),
                      textColor: Colors.white,
                      splashColor: Colors.orangeAccent,
                      color: Colors.orange.shade500,
                    ),
                  ],
                ),
                SizedBox(
                  height: 30.0,
                ),
                // 3.
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.all(0),
                        padding: const EdgeInsets.all(0),
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(207, 214, 230, 0.2),
                          border: Border.all(
                            color: Color.fromRGBO(207, 214, 230, 0.2),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(
                                  10.0) //                 <--- border radius here
                              ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 0, horizontal: 10),
                                dense: true,
                                title: Text(
                                  "Start Date",
                                  style: (TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                                ),
                                subtitle: Text(
                                    "${selectedDate.day}/${selectedDate.month}/ ${selectedDate.year} ",
                                    style: (TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16))),
                                trailing: Icon(
                                  Icons.date_range,
                                  color: Colors.lightBlue.shade800,
                                ),
                                onTap: () {
                                  _selectDate(context);
                                }),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.all(0),
                        padding: const EdgeInsets.all(0),
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(207, 214, 230, 0.2),
                          border: Border.all(
                            color: Color.fromRGBO(207, 214, 230, 0.2),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(
                                  10.0) //                 <--- border radius here
                              ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 0, horizontal: 10),
                              dense: true,
                              title: Text(
                                "Time Start",
                                style: (TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16)),
                              ),
                              subtitle: Text(
                                "${time.hour}:${time.minute}",
                                style: (TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16)),
                              ),
                              trailing: Icon(
                                Icons.access_time,
                                color: Colors.lightBlue.shade800,
                              ),
                              onTap: _pickTime,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 30.0,
                ),

                //4. remarks
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.all(0),
                        padding: const EdgeInsets.all(0),
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.black,
                              ),
                              decoration: InputDecoration(
                                alignLabelWithHint: true,
                                labelText: "Remarks",
                                labelStyle: TextStyle(fontSize: 17),
                                filled: true,
                                fillColor: Color.fromRGBO(207, 214, 230, 0.2),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 20.0),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color.fromRGBO(207, 214, 230, 0.2),
                                  ),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                              keyboardType: TextInputType.multiline,
                              maxLines: 5,
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),

                //======================================================
                //            submit button
                //======================================================
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: SizedBox(
                    width: double.infinity,
                    child: RaisedButton.icon(
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          //form is valid, proceed further

                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new OfficeAnywhere()));
                        } else {
                          setState(() {
                            _autovalidate = true; //enable realtime validation
                          });
                        }
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(20.0))),
                      label: Text(
                        'Submit',
                        style: TextStyle(
                            //fontFamily: 'BellotaText',
                            color: Colors.white,
                            letterSpacing: 3),
                      ),
                      icon: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Icon(
                          Icons.send,
                        ),
                      ),
                      textColor: Colors.white,
                      splashColor: Colors.lightBlue.shade500,
                      color: Colors.lightBlue.shade800,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //=============================================================================== Api Calling here

//CALLING STATE API HERE
// Get State information by API
  List statesList;
  String _myState;

  String stateInfoUrl = 'http://cleanions.bestweb.my/api/location/get_state';

  Future<String> _getStateList() async {
    await http.post(stateInfoUrl, headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }, body: {
      "api_key": '25d55ad283aa400af464c76d713c07ad',
    }).then((response) {
      var data = json.decode(response.body);

//      print(data);
      setState(() {
        statesList = data['state'];
      });
    });
  }
}
