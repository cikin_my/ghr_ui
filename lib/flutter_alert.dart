import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FlutterAlert extends StatefulWidget {
  @override
  _FlutterAlertState createState() => _FlutterAlertState();
}

class _FlutterAlertState extends State<FlutterAlert> {

  TextEditingController _textFieldController = TextEditingController();

  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Type Address'),
            content: TextField(
              controller: _textFieldController,
              decoration: InputDecoration(hintText: "eg:Home"),
            ),
            actions: <Widget>[
               FlatButton(
                child: new Text('CANCEL'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              RaisedButton(
                elevation: .5,
                child: Text('Add Location',style:TextStyle(color: Colors.white),),
                color: Colors.lightBlue.shade800,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),

            ],
          );
        });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Alert with form'),
      ),
      body: Center(
        child: RaisedButton(
          elevation: 5,
          child: Text('Open Alert Dialog',style:TextStyle(color: Colors.white),),
          color: Colors.blueGrey,
          onPressed: () => _displayDialog(context),
        ),
      ),
    );
  }
}
