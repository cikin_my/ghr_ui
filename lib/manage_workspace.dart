import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'animation/fade_animation.dart';
import 'landing_page.dart';

class ManageWorkspace extends StatefulWidget {
  @override
  _ManageWorkspaceState createState() => _ManageWorkspaceState();
}

class _ManageWorkspaceState extends State<ManageWorkspace> {
  //variables
  MediaQueryData queryData;

  Widget _buildAddressline2(data) {
    TextStyle _LocationTextStyle = TextStyle(
      fontFamily: "SourceSansPro",
      letterSpacing: 0.5,
      fontSize: 15,
      color: Colors.black54,
    );

    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          width: 5.0,
        ),
        Expanded(
          child: AutoSizeText(
            data,
            maxLines: 3,
            textAlign: TextAlign.left,
            style: _LocationTextStyle,
          ),
        ),
      ],
    );
  }

  Widget _buildLogitude(data) {
    queryData = MediaQuery.of(context);

    // .....   text styling  ..........
    TextStyle _LabelStyle1 = TextStyle(
      fontFamily: 'SourceSansPro',
      color: Colors.blueGrey,
      fontSize: 16.0,
    );
    TextStyle _LabelStyle2 = TextStyle(
      fontFamily: 'SourceSansPro',
      color: Colors.blueGrey.shade800,
      fontSize: 16.0,
    );
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                SizedBox(
                  width: 10.0,
                ),
                Container(
                  width: queryData.size.width / 5,
                  child: Text(
                    "Latitude ",
                    style: (_LabelStyle1),
                  ),
                ),
                Container(
                  child: Text(
                    data["Latitude"],
                    style: (_LabelStyle2),
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                SizedBox(
                  width: 10.0,
                ),
                Container(
                    width: queryData.size.width / 5,
                    child: Text("Longitude", style: (_LabelStyle1))),
                Container(
                  child: Text(data["Longitude"], style: (_LabelStyle2)),
                ),
              ],
            ),
          ],
        ),
        Row(
          children: <Widget>[
            FlatButton(
              onPressed: () => {},
              splashColor: Colors.blue.shade50,
              padding: EdgeInsets.all(10.0),
              child: Column(
                // Replace with a Row for horizontal icon + text
                children: <Widget>[
                  Icon(
                    FontAwesomeIcons.mapMarkedAlt,
                    color: Colors.green,
                  ),
                  Text(
                    "View in Map",
                    style: TextStyle(
                        color: Colors.lightBlue.shade800,
                        fontFamily: "SourceSansPro",
                        letterSpacing: 1),
                  )
                ],
              ),
            ),
          ],
        )
      ],
    );
  }

  Widget _buildLocationSection(data) {
    TextStyle _LocationTextStyle = TextStyle(
      fontFamily: "SourceSansPro",
      letterSpacing: 0.5,
      fontSize: 15,
      fontWeight: FontWeight.bold,
      color: Colors.black,
    );
    return Expanded(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: 10.0,
          ),
          Container(
            padding: const EdgeInsets.only(bottom: 8),
            child: Icon(
              Icons.pin_drop,
              color: Colors.blueGrey.shade200,
              size: 20.0,
            ),
          ),
          SizedBox(
            width: 5.0,
          ),
          Text(
            data["LocationName"],
            style: _LocationTextStyle,
          ),
        ],
      ),
    );
  }

  Widget _buildButtonSection(data) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 0.0, top: 0),
          child: ButtonTheme(
            minWidth: 50.0,
            height: 23.0,
            child: FlatButton(
              onPressed: () => {},
              splashColor: Colors.blue.shade50,
              padding: EdgeInsets.all(10.0),
              child: Row(
                // Replace with a Row for horizontal icon + text
                children: <Widget>[
                  Icon(
                    Icons.delete,
                    color: Colors.red.shade800,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Manage Workspace",
            style: TextStyle(color: Colors.white),
          ),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => Landing()));
              }),
          backgroundColor: Colors.lightBlue.shade800,
          elevation: 2,
        ),
        body: Container(
          child: Center(
            // Use future builder and DefaultAssetBundle to load the local JSON file

            child: FutureBuilder(
                future: DefaultAssetBundle.of(context)
                    .loadString('data/location.json'),
                builder: (context, snapshot) {
                  // Decode the JSON
                  var new_data = jsonDecode(snapshot.data.toString());

                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: new_data == null ? 0 : new_data.length,
                      // Build the ListView
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FadeAnimation(
                            1,
                            Card(
                              elevation: 8.5,
                              clipBehavior: Clip.antiAlias,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0)),
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    //l i n e  1
                                    Container(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          _buildLocationSection(
                                              new_data[index]),
                                          _buildButtonSection(new_data[index]),
                                        ],
                                      ),
                                    ),

                                    Divider(),

                                    //l i n e  2
                                    Container(
                                      child: _buildLogitude(new_data[index]),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }),
          ),
        ));
  }
}
