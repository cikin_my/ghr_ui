import 'package:flutter/material.dart';
import 'package:ghrui/menu.dart';

class MyServices extends StatefulWidget {
  @override
  _MyServicesState createState() => _MyServicesState();
}


class _MyServicesState extends State<MyServices> {
  double screenHeight;
  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;

    return  Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height:screenHeight / 20,
          ),

          SizedBox(
            height: 1,
          ),
          Menu()
        ],
      ),
    );
  }
}
