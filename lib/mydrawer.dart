import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ghrui/business_card.dart';
import 'package:ghrui/login.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ghrui/manage_workspace.dart';
import 'package:ghrui/my_profile.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            padding: EdgeInsets.fromLTRB(18, 10, 8, 0),
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.centerLeft,
                          child: CircleAvatar(
                            backgroundImage: AssetImage('assets/images/me.png'),
                            radius: 50.0,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      8.0, 20.0, 8.0, 0),
                                  child: Text(
                                    'Shukeri Saleh',
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontSize: 15.0,
                                        fontFamily: "OpenSans"),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      8.0, 1.0, 8.0, 0),
                                  child: Text(
                                    'Manager',
                                    style: TextStyle(
                                        color: Colors.black45,
                                        fontSize: 13.0,
                                        fontFamily: "OpenSans"),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: IconButton(
                            icon: Icon(
                              Icons.settings,
                              color: Colors.blue,
                              size: 18.0,
                            ),
                            onPressed: () {},
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    FlatButton.icon(
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) => new businessCard()));
                        },
                        icon: FaIcon(
                          FontAwesomeIcons.qrcode,
                          color: Colors.blue,
                          size: 12.0,
                        ),
                        label: Text(
                          "Digital IDs",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blue,
                              fontSize: 13.0,
                              fontFamily: "OpenSans"),
                        )),
                  ],
                )
              ],
            ),
          ),
          ListTile(
            trailing:  Icon(Icons.person_outline),
            title:  Text("My Profile"),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new MyProfile()));
            },
          ),
          new Divider(
            height: 1,
          ),
          ListTile(
            trailing:  Icon(Icons.supervised_user_circle),
            title:  Text("My Immediate Manager"),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new Login()));
            },
          ),
          Divider(
            height: 1,
          ),
          ListTile(
            trailing:  Icon(FontAwesomeIcons.mapMarkedAlt,),
            title:  Text("Manage Workspace"),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new ManageWorkspace()));
            },
          ),
          Divider(
            height: 1,
          ),
           ListTile(
            trailing:  Icon(Icons.settings_power),
            title:  Text("Log Out"),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new Login()));
            },
          ),
        ],
      ),
    );
  }
}
