import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  double screenHeight;
  final String _text1 =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Cras ut mi ut ex iaculis malesuada. ";

  Widget _buildBio(BuildContext context) {
    TextStyle bioTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: Colors.grey.shade900,
      fontSize: 15.0,
    );
    return Container(
      //color: Theme.of(context).scaffoldBackgroundColor,
      padding: EdgeInsets.all(18.0),
      child: Text(
        _text1,
        textAlign: TextAlign.center,
        style: bioTextStyle,
      ),
    );
  }
  Widget _extraContent(BuildContext context) {
    TextStyle bioTextStyle = TextStyle(
      fontFamily: 'OpenSans',
      fontWeight: FontWeight.w800,
      color: Colors.black,
      fontSize: 16.0,
    );
    return Container(
      padding: EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Self Health Declaration",
            textAlign: TextAlign.left,
            style: bioTextStyle,
          ),
          SizedBox(height: 5,),
          Text(
            "Please remember to declare your health as we transit back to work",
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 15,
              fontFamily:"OpenSans",
            ),

          ),

        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Image.asset("assets/images/home1.jpg"),
              ),
              Expanded(
                child: Container(),
              ),
              Expanded(child: Image.asset("assets/images/image_02.png"))
            ],
          ),
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                left: 28.0,
                right: 28.0,
              ),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset("assets/images/tnb_full.png",
                          width: 80.5,
                          height: 80.5,
                          color: Color.fromRGBO(255, 255, 255, 0.8),
                          colorBlendMode: BlendMode.modulate),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight / 5,
                  ),
                  Text(
                    "Welcome to  a",
                    style: TextStyle(
                        color: Colors.lightBlue.shade800,
                        fontFamily: "OpenSans",
                        fontSize: 23,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    "new TNB engagement!",
                    style: TextStyle(
                        color: Colors.lightBlue.shade800,
                        fontFamily: "OpenSans",
                        fontSize: 23,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  _buildBio(context),
                  Divider(),
                  SizedBox(
                    height: 20,
                  ),
                  _extraContent(context),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: SizedBox(
                          width: 160,
                          child: RaisedButton.icon(
                            elevation: 5,
                            onPressed: () {

                            },
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20.0))),
                            label: Text(
                              'Declare Now',
                              style: TextStyle(
                                  fontFamily: 'BellotaText',
                                  color: Colors.white,
                                  fontSize: 13,
                                  letterSpacing: 2),
                            ),
                            icon: Padding(
                              padding: EdgeInsets.all(4.0),
                              child: Icon(FontAwesomeIcons.featherAlt, size: 14,
                              ),
                            ),
                            textColor: Colors.white,
                            splashColor: Colors.redAccent,
                            color: Colors.redAccent.shade700,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
