import 'package:flutter/material.dart';
import 'package:ghrui/landing_page.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';

class MyProfile extends StatefulWidget {
  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> with TickerProviderStateMixin {

  final String _fullName = "Zhafri";
  final bool _hasIM = true;

  //styling
  TextStyle _tabLabel = TextStyle(
    fontFamily: "SourceSansPro",
    fontSize: 18,
    letterSpacing: 1,
    color: Colors.blueGrey.shade900,
  );


  Widget _buildButtons() {
    return Padding(
      padding: EdgeInsets.all(18.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: InkWell(
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: SizedBox(
                  width: double.infinity,
                  child: RaisedButton.icon(
                    onPressed: () {},
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20.0))),
                    label: Text(
                      'Update',
                      style: TextStyle(
                          //fontFamily: 'BellotaText',
                          color: Colors.white,
                          letterSpacing: 3),
                    ),
                    icon: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Icon(
                        Icons.mode_edit,
                      ),
                    ),
                    textColor: Colors.white,
                    splashColor: Colors.lightBlue.shade500,
                    color: Colors.lightBlue.shade800,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  Widget _buildButtonsEdit() {
    return Padding(
      padding: EdgeInsets.all(18.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: InkWell(
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: SizedBox(
                  width: double.infinity,
                  child: RaisedButton.icon(
                    onPressed: () {},
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20.0))),
                    label: Text(
                      'Edit',
                      style: TextStyle(
                        //fontFamily: 'BellotaText',
                          color: Colors.white,
                          letterSpacing: 3),
                    ),
                    icon: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Icon(
                        FontAwesomeIcons.userEdit,size: 15,
                      ),
                    ),
                    textColor: Colors.white,
                    splashColor: Colors.lightBlue.shade500,
                    color: Colors.lightBlue.shade800,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  Widget _buildProfileImage() {
    return Center(
      child: Container(
        width: 80.0,
        height: 80.0,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/me.png'),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.circular(100.0),
          border: Border.all(
            color: Colors.white,
            width: 4.0,
          ),
        ),
      ),
    );
  }
  Widget _buildIMName() {
    TextStyle _nameTextStyle = TextStyle(
      //fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 17.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _emailTextStyle = TextStyle(
      //fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 14.0,
      fontWeight: FontWeight.normal,
    );

    return Row(
      children: <Widget>[
        Column(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: CircleAvatar(
                  backgroundImage: AssetImage('assets/images/boss.jpg'),
                  radius: 25.0,
                ),
              ),
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Text(
                "Shuk",
                style: _nameTextStyle,
              ),
            ),
            Text(
              "mkshukeri@gmail.com",
              style: _emailTextStyle,
            ),
          ],
        ),
      ],
    );
  }
  Widget _BuildProfileForm() {
    //=====================================
    // Input form styling :
    //=====================================
    TextStyle _InputText = TextStyle(
      fontSize: 17,
      letterSpacing: 1,
      color: Colors.blueGrey.shade900,
    );

    InputDecoration _inputDecoration(_LabelName, _Icon) {
      TextStyle _formLabel = TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w400,
        letterSpacing: .5,
        color: Colors.blueGrey.shade900,
      );
      return InputDecoration(
          prefixIcon: Icon(
            _Icon,
            size: 15,
          ),
          labelText: _LabelName,
          labelStyle: _formLabel,
          filled: true,
          fillColor: Color.fromRGBO(207, 214, 230, 0.2),
          contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20.0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color.fromRGBO(207, 214, 230, 0.2),
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          suffixIcon: Icon(
            Icons.edit,
            size: 17,
            color: Colors.grey.withOpacity(.4),
          ));
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 20.0,
          ),
          //Name:
          Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                style: _InputText,
                decoration: _inputDecoration("Name", FontAwesomeIcons.userTie),
              ),
            ),
          ),
          //position+job
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      style: _InputText,
                      decoration: _inputDecoration("Position", Icons.event_seat),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      style: _InputText,
                      decoration: _inputDecoration(
                          "Job Title", FontAwesomeIcons.blackTie),
                    ),
                  ),
                ),
              ),
            ],
          ),
          //Department
          Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                style: _InputText,
                decoration:
                    _inputDecoration("Department", FontAwesomeIcons.networkWired),
              ),
            ),
          ),
          //Division
          Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                style: _InputText,
                decoration: _inputDecoration("Division", Icons.group),
              ),
            ),
          ),
          //Office Address
          Container(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 8, 4),
              child: TextFormField(
                style: _InputText,
                decoration: _inputDecoration("Office Address", Icons.map),
              ),
            ),
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 8, 4),
              child: TextFormField(
                style: _InputText,
                decoration: _inputDecoration("State", Icons.pin_drop),
              ),
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8, 4, 8, 8),
                    child: TextFormField(
                      style: _InputText,
                      decoration: _inputDecoration("City", Icons.pin_drop),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8, 4, 8, 8),
                    child: TextFormField(
                      style: _InputText,
                      decoration: _inputDecoration("zip", Icons.pin_drop),
                    ),
                  ),
                ),
              ),
            ],
          ),
          //phone no
          Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                style: _InputText,
                decoration: _inputDecoration("Phone Number", Icons.phone_android),
              ),
            ),
          ),
          //office No
          Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                style: _InputText,
                decoration: _inputDecoration("Office Number", Icons.phone),
              ),
            ),
          ),

          _buildButtons(),
        ],
      ),
    );
  }
  Widget _BuildIMForm() {
    //=====================================
    // Input form styling :
    //=====================================
    TextStyle _InputText = TextStyle(
      fontSize: 17,
      letterSpacing: 1,
      color: Colors.blueGrey.shade900,
    );

    InputDecoration _inputDecoration(_LabelName, _Icon) {
      TextStyle _formLabel = TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w400,
        letterSpacing: .5,
        color: Colors.blueGrey.shade900,
      );
      return InputDecoration(
          prefixIcon: Icon(
            _Icon,
            size: 15,
          ),
          labelText: _LabelName,
          labelStyle: _formLabel,
          filled: true,
          fillColor: Color.fromRGBO(207, 214, 230, 0.2),
          contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20.0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color.fromRGBO(207, 214, 230, 0.2),
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          suffixIcon: Icon(
            Icons.edit,
            size: 17,
            color: Colors.grey.withOpacity(.4),
          ));
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 20.0,
          ),
          //IM:
          Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                style: _InputText,
                decoration: _inputDecoration("Immediate Manager", Icons.supervised_user_circle),
              ),
            ),
          ),

          //Emp No
          Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                style: _InputText,
                decoration:
                _inputDecoration("Employee No", FontAwesomeIcons.idCard),
              ),
            ),
          ),



        ],
      ),
    );
  }
  Widget _BuildIM() {
    //=====================================
    // Input form styling :
    //=====================================
    TextStyle _InputText = TextStyle(
      fontSize: 17,
      letterSpacing: 1,
      color: Colors.blueGrey.shade900,
    );

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 20.0,
            ),
            //IM:
            Text("Immediate Manager",style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),),
            _buildIMName(),
            _buildButtonsEdit(),




          ],
        ),
      ),
    );
  }

  //================================================
  //main
  //================================================
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text('Edit Profile', style: _tabLabel,),
          iconTheme: new IconThemeData(color: Colors.lightBlue.shade800),
          backgroundColor: Colors.white,
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      _buildProfileImage(),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              _fullName,
                              style: _tabLabel,
                            ),
                          ),
                          SizedBox(height: 20,),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          bottom:
          PreferredSize(
            preferredSize: Size.square(140.0),
            child: TabBar(
              isScrollable: true,
              unselectedLabelColor: Colors.grey,
              labelColor: Colors.white,
              indicatorSize: TabBarIndicatorSize.tab,
              indicator: BubbleTabIndicator(
                indicatorHeight: 29.0,
                indicatorColor: Colors.lightBlue.shade800,
                tabBarIndicatorSize: TabBarIndicatorSize.tab,
              ),
              tabs: [
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.card_giftcard, size: 15,),
                        SizedBox(width: 5.0,),
                        Text("Business Card",),
                      ],
                    ),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.supervised_user_circle, size: 15,),
                        SizedBox(width: 5.0,),
                        Text("Employee",),
                      ],
                    ),
                  ),
                ),

              ],

            ),
          ),

        ),
        body: TabBarView(children: [
         SingleChildScrollView(child: _BuildProfileForm()),
         SingleChildScrollView(child:(_hasIM)
             ? _BuildIM()
             : _BuildIMForm()
             ,

        ),

        ]),
      ),
    );
  }
}
