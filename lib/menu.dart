import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ghrui/checkin.dart';
import 'package:ghrui/my_team.dart';
import 'package:ghrui/office_anywhere.dart';

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  List<String> events = [
    "digitalID",
    "businessCard",
    "ESHD",
    "officeAnywhere",
    "checkIn",
    "myTeam",
  ];

  @override
  Widget build(BuildContext context) {
    var color = 0xFFF5F5F5;
    return Flexible(
      child: GridView(
        padding: EdgeInsets.only(left: 16, right: 16),
        physics: BouncingScrollPhysics(), //only for ios
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        children: events.map((url) {
          return GestureDetector(
            child: Container(
              decoration: BoxDecoration(
                  color: Color(color),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[200],
                      blurRadius: 2.0, // has the effect of softening the shadow
                      spreadRadius:
                          1.0, // has the effect of extending the shadow
                      offset: Offset(
                        3.0, // horizontal, move right 10
                        3.0, // vertical, move down 10
                      ),
                    )
                  ]),
              margin: const EdgeInsets.all(10.0),
              child: getCardByTitle(url),
            ),
            onTap: () {
              Navigator.of(context).pop();
              switch (url) {
                case "digitalID":
                  {
                    // statements;
                  }
                  break;

                case "businessCard":
                  {
                    //statements;
                  }
                  break;
                case "ESHD":
                  {
                    //statements;
                  }
                  break;
                case "officeAnywhere":
                  {
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) =>
                            new OfficeAnywhere()));
                  }
                  break;
                case "checkIn":
                  {
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) => new CheckInOut()));
                  }
                  break;
                case "myTeam":
                  {

                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) => new MyTeam()));
                  }
                  break;

                default:
                  {
                    //statements;
                    Navigator.of(context).pop();
                  }
                  break;
              }
              if (url == "officeAnywhere") {}

//                print(url);
            },
          );
        }).toList(),
      ),
    );
  }
}

Column getCardByTitle(String url) {
  String img = "";
  String title = "";
  if (url == "digitalID") {
    img = "assets/images/declaration.png";
    title = "My Digital ID";
  } else if (url == "businessCard") {
    img = "assets/images/qr-scan.png";
    title = "My Business Card";
  } else if (url == "ESHD") {
    img = "assets/images/checkinout.png";
    title = "ESHD";
  } else if (url == "officeAnywhere") {
    img = "assets/images/location.png";
    title = "Office Anywhere";
  } else if (url == "checkIn") {
    img = "assets/images/declaration.png";
    title = "Check In/Out";
  } else {
    img = "assets/images/teamwork.png";
    title = "My Team";
  }

  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Image.asset(
        img,
        width: 45,
      ),
      SizedBox(
        height: 10,
      ),
      Text(
        title,
        style: TextStyle(
            color: Colors.black87, fontSize: 16, fontWeight: FontWeight.w600),
      ),
//      SizedBox(
//        height: 8,
//      ),
//      Text(
//        subtitle,
//        style: TextStyle(
//            color: Colors.black45, fontSize: 12, fontWeight: FontWeight.w600),
//      ),
    ],
  );
}
