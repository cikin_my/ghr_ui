import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ghrui/homepage.dart';
import 'package:ghrui/homepage2.dart';
import 'package:ghrui/my_approval.dart';
import 'package:ghrui/my_approval2.dart';
import 'package:ghrui/my_services.dart';
import 'menu.dart';
import 'mydrawer.dart';

class Landing extends StatefulWidget {

  @override
  _LandingState createState() => _LandingState();
}

class _LandingState extends State<Landing> {

  int _currentIndex = 1;
  final List<Widget> _children = [
    HomePage2(),
    MyServices(),

  ];

  void onTappedBar(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  double screenHeight;
  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text(
          'Welcome Back!',
          style: TextStyle(
            fontWeight: FontWeight.w900,
            color: Colors.black87,
              fontFamily: "OpenSans",
            fontSize: 14
          ),
        ),
        backgroundColor: Colors.white70,
        actions: <Widget>[
          IconButton(
            icon: FaIcon(
              FontAwesomeIcons.thLarge,
              size: 14,
              color: Colors.blue.shade700,
            ),
            tooltip: 'Show Snackbar',
            onPressed: () {},
          ),
          IconButton(
            icon: FaIcon(
              FontAwesomeIcons.expand,
              size: 14,
              color: Colors.blue.shade700,
            ),
            tooltip: 'Next page',
            onPressed: () {},
          ),
        ],
        leading: Builder(
          builder: (context) => GestureDetector(
          child: const Padding(
            padding: EdgeInsets.all(8.0),
            child: ClipOval(
              child: Image(
                image: AssetImage('assets/images/me.png'),
              ),

            ),
          ),
          onTap:(){
            Scaffold.of(context).openDrawer();
          },
        ),
        ),


      ),
      drawer: MyDrawer(),
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
          onTap: onTappedBar,
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(
              icon: new Icon(Icons.home),
              title: new Text('Home'),
            ),
            BottomNavigationBarItem(
              icon: new Icon(Icons.dashboard),
              title: new Text('My Services'),
            ),

          ]),
    );


  }
}
