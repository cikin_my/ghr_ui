import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ghrui/landing_page.dart';
import 'package:ghrui/modalPopup/custom_dialog.dart';
import 'animation/fade_animation.dart';
import 'package:intl/intl.dart';

class Checkin_old extends StatefulWidget {
  @override
  _Checkin_oldState createState() => _Checkin_oldState();
}

class _Checkin_oldState extends State<Checkin_old> {


  Widget _buildDateSection(data) {
    TextStyle _DateTextStyle = TextStyle(
      fontFamily: 'SourceSansPro',
      color: Colors.black54,
      fontWeight: FontWeight.bold,
      fontSize: 14,
    );

    TextStyle _DayTextStyle = TextStyle(
      color: Colors.black87,
      fontWeight: FontWeight.bold,
      fontSize: 27,
      fontFamily: "BellotaText",
      letterSpacing: 2,
    );


    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Container(
          child: Column(
        children: <Widget>[
          Text(
            DateFormat('E').format(DateTime.parse(data['Date'])),
            style: _DateTextStyle,
          ),
          Text(
            DateFormat('dd').format(DateTime.parse(data['Date'])),
            style: _DayTextStyle,
          ),
          Text(
            DateFormat('MMM yyyy').format(DateTime.parse(data['Date'])),
            style: TextStyle(
              color: Colors.black54,
              fontWeight: FontWeight.bold,
              fontSize: 12,
            ),
          ),
        ],
      )),
    );
  }

  Widget _builCheckin_oldSection(data) {
    TextStyle _LocationTextStyle = TextStyle(
      fontFamily: "SourceSansPro",
      letterSpacing:0.5,
      fontSize: 15,
      fontWeight: FontWeight.bold,
      color: Colors.black,
    );
    TextStyle _CheckInTextStyle = TextStyle(
      fontFamily: 'SourceSansPro',
      color: Colors.blueGrey,
      fontSize: 16.0,
    );
    TextStyle _TimeTextStyle = TextStyle(
      fontFamily: 'SourceSansPro',
      color: Colors.blueGrey.shade800,
      fontSize: 16.0,
    );

    return Expanded(
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [
            Color.fromRGBO(253, 251, 251, 1),
            Color.fromRGBO(227, 234, 242,.7)
          ],
          stops: [0.0, 0.7],
        )),
        child: Padding(
          //**changes start here
          padding: const EdgeInsets.fromLTRB(0, 15, 15, 8),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 10.0,
                          ),
                          Icon(
                            Icons.pin_drop,
                            color: Colors.lightBlue,
                            size: 20.0,
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Text(
                            data['locationName'],
                            style: _LocationTextStyle,
                          ),

                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  //**changes start here
                  Expanded(
                    child: MaterialButton(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(8,8,0,8),
                        child: Column(
                          children: <Widget>[
                            Text(
                              "Check In",
                              style: (_CheckInTextStyle),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(3.0),
                              child: Text(
                                data["checkInTime"],
                                style: (_TimeTextStyle),
                              ),
                            ),
                          ],
                        ),
                      ),
                      elevation: 0,
                      splashColor: Color.fromRGBO(244,248,252,1),
                      onPressed: (){
                        showDialog(
                          context: context,
                          builder: (BuildContext context) => CustomDialog(
                            title: "Check In",
                            description:
                            "Are you sure you want to Check In now?",
                            primaryButtonText: "Yes",
                            primaryButtonRoute: "/",
                            secondaryButtonText: "Cancel",
                            secondaryButtonRoute: "/check_in_out", //go somewhere
                          ),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top:10.0),
                    child: Container(
                        height: 40,
                        child: VerticalDivider(
                          color: Color.fromRGBO(231, 151, 150, .5),
                          thickness: 1,
                        )),
                  ),
                  Expanded(
                    child: MaterialButton(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                        child: Column(
                          children: <Widget>[
                            Text("Check Out", style: (_CheckInTextStyle)),
                            Text("--:-- ", style: (_TimeTextStyle)),
                          ],
                        ),
                      ),
                      elevation: 0,
                      splashColor: Color.fromRGBO(244,248,252,1),
                      onPressed: () {
                        //confirm checkout
                        showDialog(
                          context: context,
                          builder: (BuildContext context) => CustomDialog(
                            title: "Check Out",
                            description:
                                "Are you sure you want to Check Out now?",
                            primaryButtonText: "Yes",
                            primaryButtonRoute: "/",
                            secondaryButtonText: "Cancel",
                            secondaryButtonRoute: "/check_in_out", //go somewhere
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "CheckOut",
            style: TextStyle(color: Colors.white),
          ),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => Landing()));
              }),
          backgroundColor: Colors.lightBlue.shade800,
          elevation: 2,
        ),
        body: Container(
          child: Center(
            // Use future builder and DefaultAssetBundle to load the local JSON file

            child: FutureBuilder(
                future: DefaultAssetBundle.of(context)
                    .loadString('data/sampleCheckin.json'),
                builder: (context, snapshot) {
                  // Decode the JSON
                  var new_data = jsonDecode(snapshot.data.toString());

                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: new_data == null ? 0 : new_data.length,
                      // Build the ListView
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          margin: new EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Padding(
                            padding: const EdgeInsets.all(0),
                            child: FadeAnimation(
                              1,
                              Card(
                                shadowColor: Colors.black38,
                                elevation: 5.5,
                                clipBehavior: Clip.antiAlias,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0)),
                                color: Colors.white,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                        padding: EdgeInsets.all(0),
                                        margin: EdgeInsets.all(0),
                                        height: 80,
                                        child: VerticalDivider(
                                          thickness: 3,
                                          color: Colors.lightBlue.shade500,
                                        )),
                                    //========================================================
                                    //            1.date
                                    //========================================================
                                    _buildDateSection(new_data[index]),
                                    //========================================================
                                    //2. location,check
                                    //========================================================
                                    _builCheckin_oldSection(new_data[index]),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }),
          ),
        ));
  }
}
